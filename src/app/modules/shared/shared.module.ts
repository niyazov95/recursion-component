import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {MaterialModule} from '../material/material.module';
import {CategoryRecursionModelComponent} from './components/category-tree-list/category-recursion-model/category-recursion-model.component';
import {CategoryTreeListComponent} from './components/category-tree-list/category-tree-list.component';

@NgModule({
  declarations: [CategoryTreeListComponent, CategoryRecursionModelComponent],
  imports: [CommonModule, MaterialModule, FormsModule],
})
export class SharedModule {}
