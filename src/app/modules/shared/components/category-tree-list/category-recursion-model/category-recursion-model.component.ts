import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CategoryModel} from '../../../models/category.model';

@Component({
  selector: 'app-category-recursion-model',
  templateUrl: './category-recursion-model.component.html',
  styleUrls: ['./category-recursion-model.component.scss'],
})
export class CategoryRecursionModelComponent implements OnInit {
  @Input() categories: CategoryModel[];
  @Input() multiple: boolean;
  @Input() editable: boolean;
  @Output() $getChild = new EventEmitter<CategoryModel>();
  @Output() selectedCategory = new EventEmitter();
  @Input() multiSelected: CategoryModel[];

  constructor() {}

  ngOnInit() {
    if (!this.multiSelected) {
      this.multiSelected = [];
    }
  }

  getChild(c: CategoryModel) {
    this.$getChild.emit(c);
  }

  selectCategory(category: CategoryModel, checked?: boolean) {
    if (!this.multiple) {
      this.selectedCategory.emit(category);
    } else {
      if (checked) {
        category.checked = true;
        this.multiSelected.push(category);
      } else {
        category.checked = false;
        this.multiSelected.splice(this.multiSelected.indexOf(category), 1);
      }
      this.selectedCategory.emit([Array.from(new Set(this.multiSelected))]);
    }
  }
}
