import {ID} from '@datorama/akita';

export interface ISuccess<DATA_TYPE> {
  id: ID;
  endpoint: string;
  data: DATA_TYPE;
}
