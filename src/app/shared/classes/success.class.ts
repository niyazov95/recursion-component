import {ISuccess} from '../interfaces/success.interface';
import {guid, ID} from '@datorama/akita';

export class Success<RESPONSE_TYPE> implements ISuccess<RESPONSE_TYPE> {
  constructor(data: RESPONSE_TYPE, endpoint: string, id: ID = guid()) {
    this.data = data;
    this.endpoint = endpoint;
    this.id = id;
  }
  data: RESPONSE_TYPE;
  endpoint: string;
  id: ID;
}
